# Secret Santa

Simple script I wrote one year so my friends spread across different colleges could organize a secret Santa without someone missing out on the fun. Rehosting it on gitlab because it is fun to play around with.
The emails will be in the sent mail box of the person who runs the script in case something goes wrong. There is a good change the emails will go to the spam folder so check there if it does not work.

# How to use

1. Change `EMAIL` and `PASSWORD` variables at the beginning of the file. PASSWORD may need to be an app specific password if you have 2-factor authentication on.
2. In the same directory have a file named `emails.csv` with the names and emails of everyone participating (including yourself).
3. Run a test by executing `python secretSanta.py`
4. If that works then change `DEBUG` to be `False` and run again. This will send emails to all the participants with their match.
